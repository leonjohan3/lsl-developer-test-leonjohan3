package au.com.qantas.loyalty.lsl.candidatetask.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({
    "programId",
    "marketingName",
    "summaryDescription"
})
public class Program {

  private String programId;

  private String marketingName;

  private String summaryDescription;
}
