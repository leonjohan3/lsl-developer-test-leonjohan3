package au.com.qantas.loyalty.lsl.candidatetask.repository;

import org.springframework.data.repository.CrudRepository;

public interface ProgramRepository extends CrudRepository<ProgramDAO, String> {


}
