package au.com.qantas.loyalty.lsl.candidatetask.service;

import au.com.qantas.loyalty.lsl.candidatetask.model.AccountStatus;
import au.com.qantas.loyalty.lsl.candidatetask.model.Member;
import au.com.qantas.loyalty.lsl.candidatetask.model.Program;
import au.com.qantas.loyalty.lsl.candidatetask.repository.MemberDAO;
import au.com.qantas.loyalty.lsl.candidatetask.repository.MemberProgramDAO;
import au.com.qantas.loyalty.lsl.candidatetask.repository.MemberProgramRepository;
import au.com.qantas.loyalty.lsl.candidatetask.repository.MemberRepository;
import au.com.qantas.loyalty.lsl.candidatetask.repository.ProgramDAO;
import au.com.qantas.loyalty.lsl.candidatetask.repository.ProgramRepository;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MemberService {

  private static final String FREQUENT_FLYER_PROGRAM = "FF";

  private MemberRepository memberRepository;

  private ProgramRepository programRepository;

  private MemberProgramRepository memberProgramRepository;

  @Autowired
  public MemberService(
      final MemberRepository memberRepository,
      final ProgramRepository programRepository,
      final MemberProgramRepository memberProgramRepository) {

    this.memberRepository = memberRepository;
    this.programRepository = programRepository;
    this.memberProgramRepository = memberProgramRepository;
  }

  public Member createMember(final Member member) {

    log.info("Creating a member");

    final MemberDAO memberDAO = MemberDAO.builder()
        .accountStatus(AccountStatus.PENDING.name())
        .givenName(member.getFirstName())
        .surname(member.getLastName())
        .enrolledSince(LocalDate.now())
        .build();

    final MemberDAO savedMember = memberRepository.save(memberDAO);

    log.info("Created a member with id '{}'", savedMember.getMemberId());

    final Optional<ProgramDAO> programToEnrolMemberIn = programRepository.findById(FREQUENT_FLYER_PROGRAM);

    final Set<Program> enrolledPrograms = new HashSet<>();

    if (programToEnrolMemberIn.isPresent()) {
      memberProgramRepository.save(MemberProgramDAO.builder()
          .memberId(memberDAO.getMemberId())
          .programId(programToEnrolMemberIn.get().getProgramId())
          .build());

      enrolledPrograms.add(Program.builder()
          .programId(programToEnrolMemberIn.get().getProgramId())
          .marketingName(programToEnrolMemberIn.get().getProgramName())
          .summaryDescription(programToEnrolMemberIn.get().getProgramDescription())
          .build());
    }

    return Member.builder()
        .memberId(savedMember.getMemberId())
        .accountStatus(AccountStatus.valueOf(savedMember.getAccountStatus()))
        .enrolledPrograms(enrolledPrograms)
        .firstName(savedMember.getGivenName())
        .lastName(savedMember.getSurname())
        .memberSince(savedMember.getEnrolledSince())
        .build();
  }

  public Optional<Member> getMember(final Long memberId) {

    log.info("Finding a member by id '{}'", memberId);

    final Optional<MemberDAO> optionalFoundMemberDAO = memberRepository.findById(memberId);

    if (!optionalFoundMemberDAO.isPresent()) {
      return Optional.empty();
    }

    log.info("Found a member by id '{}'", memberId);

    final MemberDAO foundMemberDAO = optionalFoundMemberDAO.get();

    final Set<String> programIds = new HashSet<>();

    final Iterable<MemberProgramDAO> memberProgramDAOS = memberProgramRepository.findAllByMemberId(foundMemberDAO.getMemberId());

    memberProgramDAOS.forEach(mp -> programIds.add(mp.getProgramId()));

    final Set<Program> enrolledPrograms = new HashSet<>();

    programRepository.findAllById(programIds).forEach(p ->
        enrolledPrograms.add(Program.builder()
            .programId(p.getProgramId())
            .marketingName(p.getProgramName())
            .summaryDescription(p.getProgramDescription())
            .build()));

    return Optional.of(Member.builder()
        .memberId(foundMemberDAO.getMemberId())
        .accountStatus(AccountStatus.valueOf(foundMemberDAO.getAccountStatus()))
        .enrolledPrograms(enrolledPrograms)
        .firstName(foundMemberDAO.getGivenName())
        .lastName(foundMemberDAO.getSurname())
        .memberSince(foundMemberDAO.getEnrolledSince())
        .build());
  }
}
