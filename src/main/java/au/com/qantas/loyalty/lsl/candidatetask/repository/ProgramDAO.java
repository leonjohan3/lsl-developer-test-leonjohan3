package au.com.qantas.loyalty.lsl.candidatetask.repository;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Program")
public class ProgramDAO {

  @Id
  @Column(name = "programId", updatable = false, nullable = false)
  @NotNull
  private String programId;

  @NotNull
  @Column(name = "programName", nullable = false)
  private String programName;

  @NotNull
  @Column(name = "programDescription", nullable = false)
  private String programDescription;

}
