package au.com.qantas.loyalty.lsl.candidatetask.api;

import au.com.qantas.loyalty.lsl.candidatetask.model.Program;
import au.com.qantas.loyalty.lsl.candidatetask.service.ProgramService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/program")
public class ProgramController {

  private final ProgramService programService;

  @Autowired
  ProgramController(final ProgramService programService) {
    this.programService = programService;
  }

  @GetMapping(value = "/{programId}")
  public Program getMember(@PathVariable final String programId) {

    final Optional<Program> foundProgram = programService.getProgram(programId);

    if (!foundProgram.isPresent()) {
      throw new ResourceNotFoundException("No program exists with programId=" + programId);
    }

    return foundProgram.get();
  }

}
