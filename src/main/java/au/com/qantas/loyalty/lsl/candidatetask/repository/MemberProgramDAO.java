package au.com.qantas.loyalty.lsl.candidatetask.repository;

import au.com.qantas.loyalty.lsl.candidatetask.repository.MemberProgramDAO.MemberProgramId;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Member_Program")
@IdClass(MemberProgramId.class)
public class MemberProgramDAO implements Serializable {

  @Id
  @NotNull
  private Long memberId;

  @Id
  @NotNull
  private String programId;

  @Data
  @Builder
  @AllArgsConstructor
  @NoArgsConstructor
  public static class MemberProgramId implements Serializable {
    @Column(name = "memberId", nullable = false)
    private Long memberId;
    @Column(name = "programId", nullable = false)
    private String programId;
  }
}
