package au.com.qantas.loyalty.lsl.candidatetask.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@AllArgsConstructor
@JsonPropertyOrder({
    "memberId",
    "accountStatus",
    "firstName",
    "lastName",
    "memberSince",
    "enrolledPrograms"
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Member implements Serializable {

  @JsonCreator
  public Member(
      @JsonProperty("firstName") String firstName,
      @JsonProperty("lastName") String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  @JsonProperty(access = Access.READ_ONLY)
  private Long memberId;

  @JsonProperty(access = Access.READ_ONLY)
  private AccountStatus accountStatus;

  private String firstName;

  private String lastName;

  @JsonProperty(access = Access.READ_ONLY)
  private LocalDate memberSince;

  @JsonProperty(access = Access.READ_ONLY)
  private Set<Program> enrolledPrograms;
}
