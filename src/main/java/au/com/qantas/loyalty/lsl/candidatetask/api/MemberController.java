package au.com.qantas.loyalty.lsl.candidatetask.api;

import au.com.qantas.loyalty.lsl.candidatetask.model.Member;
import au.com.qantas.loyalty.lsl.candidatetask.service.MemberService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/member")
public class MemberController {

  private final MemberService memberService;

  @Autowired
  MemberController(final MemberService memberService) {
    this.memberService = memberService;
  }

  @PostMapping(value = {"", "/"})
  public Member createMember(@RequestBody final Member member) {

    final Member createdMember = memberService.createMember(member);
    return createdMember;
  }

  @GetMapping(value = "/{memberId}")
  public Member getMember(@PathVariable final Long memberId) {

    final Optional<Member> foundMember = memberService.getMember(memberId);

    if (!foundMember.isPresent()) {
      throw new ResourceNotFoundException("No member exists with memberId=" + memberId);
    }

    return foundMember.get();
  }

}
