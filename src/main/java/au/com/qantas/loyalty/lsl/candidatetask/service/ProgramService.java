package au.com.qantas.loyalty.lsl.candidatetask.service;

import au.com.qantas.loyalty.lsl.candidatetask.model.Program;
import au.com.qantas.loyalty.lsl.candidatetask.repository.ProgramDAO;
import au.com.qantas.loyalty.lsl.candidatetask.repository.ProgramRepository;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ProgramService {

  public static final String FREQUENT_FLYER_PROGRAM = "FF";

  private ProgramRepository programRepository;

  @Autowired
  public ProgramService(final ProgramRepository programRepository) {
    this.programRepository = programRepository;
  }

  public Optional<Program> getProgram(final String programId) {

    log.info("Finding a program by id '{}'", programId);

    final Optional<ProgramDAO> optionalFoundProgramDAO = programRepository.findById(programId);

    if (!optionalFoundProgramDAO.isPresent()) {
      return Optional.empty();
    }

    log.info("Found a program by id '{}'", programId);

    final ProgramDAO foundProgramDAO = optionalFoundProgramDAO.get();

    return Optional.of(Program.builder()
        .programId(foundProgramDAO.getProgramId())
        .marketingName(foundProgramDAO.getProgramName())
        .summaryDescription(foundProgramDAO.getProgramDescription())
        .build());
  }
}
