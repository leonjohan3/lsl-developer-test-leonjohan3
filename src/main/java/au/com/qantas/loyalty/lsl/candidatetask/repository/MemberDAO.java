package au.com.qantas.loyalty.lsl.candidatetask.repository;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Member")
@SequenceGenerator(name="memberIdGenerator", initialValue=1900100030, allocationSize=100)
public class MemberDAO implements Serializable {

  @Id
  @GeneratedValue(generator = "memberIdGenerator")
  @Column(name = "memberId", updatable = false, nullable = false)
  @NotNull
  private Long memberId;

  @NotNull
  @Column(name = "accountStatus", nullable = false)
  private String accountStatus;

  @NotNull
  @Column(name = "givenName", nullable = false)
  private String givenName;

  @NotNull
  @Column(name = "surname", nullable = false)
  private String surname;

  @NotNull
  @Column(name = "enrolledSince", nullable = false)
  private LocalDate enrolledSince;
}
