package au.com.qantas.loyalty.lsl.candidatetask.repository;

import au.com.qantas.loyalty.lsl.candidatetask.repository.MemberProgramDAO.MemberProgramId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface MemberProgramRepository extends CrudRepository<MemberProgramDAO, MemberProgramId> {

  @Query("SELECT mp FROM MemberProgramDAO mp WHERE mp.memberId = :memberId")
  Iterable<MemberProgramDAO> findAllByMemberId(@Param("memberId") final Long memberId);
}
