
# Candidate coding task

Take a small Spring Boot micro-service and modify it to add some functionality.

Along the way please note any improvements you would suggest that could be made to improve the 
reliability, maintainability, testability, architecture, or ease of understanding, 

## Task summary

* Clone this Git repository.
* Build using the provided Maven pom file.
* Execute Spring Boot app to discover current service endpoints.
* Create unit tests for new functionality.
* Make changes in the IDE or editor of your choice.
* Submit modified application via a pull-request.

## Task details

### Application background

This micro-service manages frequent flyer loyalty program members. 

Members are frequent flyer customers who have a loyalty card and they collect points for
flights-taken with our airline as well as for purchases made with partner retailers.

A member can be enrolled in one or more programs that provide different kinds of benefits
or rewards. 

### Task requirements

Please remember that there are no right or wrong implementations – 
each is just different from another.

We use this task as an entry point to conversations around coding.

#### Task 1 – member enrollment into a program

Create a new service endpoint that allows a client to 
enrol a member into a program.

#### Task 2 – add address information to member

We need to mail information packs to members and to do this we require 
member address information. 

Update or create a service that allows for address information to be 
stored against a member.

Address information should contain:

* address line 1
* address line 2
* city/town/locality
* postcode/zipcode
* state/province/county
* country

## Other information

This application uses a H2 in memory database. You can use
the H2 console to browse what schema and data is in the database.

### Browse the database contents

With the Spring Boot application running, browse to:

* http://localhost:8080/h2-console

Then connect using the following:

```
Saved Settings: Generic H2 (Embedded)
Setting Name:   Generic H2 (Embedded)
-------------------------------------
Driver Class:   org.h2.Driver
JDBC URL:       jdbc:h2:mem:test
User Name:      sa
Password:     
```

### Database initialisation

The Spring Boot application initialises the database with the data found in 
the resource file:

```
src/main/resources/data.sql
```

### Existing service endpoints

#### Get member by id

Gets member information by the member id.

```
HTTP GET /member/{memberId} 
```

#### Create a new member

Create a new member using a HTTP POST. Example payload shown below:

```
HTTP POST /member/

{
    "firstName": "Charles", 
    "lastName":  "Dickens"
}
```

#### Get program by id

Gets program information by the program id.

```
HTTP GET /program/{programId} 
```
